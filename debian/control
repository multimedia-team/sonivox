Source: sonivox
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Dennis Braun <snd@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 libgtest-dev <!nocheck>
Homepage: https://github.com/pedrolcl/sonivox
Vcs-Browser: https://salsa.debian.org/multimedia-team/sonivox
Vcs-Git: https://salsa.debian.org/multimedia-team/sonivox.git
Standards-Version: 4.7.0
Rules-Requires-Root: no

Package: libsonivox3
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Sonivox Embedded Audio Synthesis Library
 Fork of the AOSP 'platform_external_sonivox' to use out of Android.
 .
 This is a Wave Table synthesizer, not using external soundfont files
 but embedded samples instead. It is also a real time GM synthesizer.
 .
 It consumes very little resources, so it may be indicated in projects
 for small embedded devices.
 There is neither MIDI input nor Audio output facilities included in
 the library. You need to provide your own input/output.

Package: libsonivox-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libsonivox3 (= ${binary:Version}),
 ${misc:Depends}
Description: Sonivox Embedded Audio Synthesis Library (Development files)
 Fork of the AOSP 'platform_external_sonivox' to use out of Android.
 .
 This is a Wave Table synthesizer, not using external soundfont files
 but embedded samples instead. It is also a real time GM synthesizer.
 .
 It consumes very little resources, so it may be indicated in projects
 for small embedded devices.
 There is neither MIDI input nor Audio output facilities included in
 the library. You need to provide your own input/output.
 .
 This package contains the development files of Sonivox.

Package: sonivoxrender
Architecture: any
Depends:
 libsonivox3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: Sonivox Embedded Audio Synthesis Library - Render Tool
 Fork of the AOSP 'platform_external_sonivox' to use out of Android.
 .
 This is a Wave Table synthesizer, not using external soundfont files
 but embedded samples instead. It is also a real time GM synthesizer.
 .
 It consumes very little resources, so it may be indicated in projects
 for small embedded devices.
 There is neither MIDI input nor Audio output facilities included in
 the library. You need to provide your own input/output.
 .
 This package contains the render tool of Sonivox.
